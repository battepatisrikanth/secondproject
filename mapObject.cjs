
function mapObject(testObject,callback){
    let keysArray=[];

    if(testObject== null || testObject == undefined){
        return keysArray;
    }
    if(typeof(testObject)!="object"){
        return keysArray;
    }
    function callback(value){
        value+=5;
        return value;
    }
 
    for(let key in testObject){
        if(typeof(testObject[key])!=="function"){
            testObject[key]=callback(testObject[key]);
        } else {
            testObject[key] = testObject[key]();
        }
    }

    return testObject;
}

module.exports=mapObject;