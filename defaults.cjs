
function defaults(testObject,defaultObject){
    if(testObject== null || testObject == undefined || typeof(testObject)!="object"||Array.isArray(testObject)){
        return {};
    }
    if( defaultObject == null || defaultObject==undefined|| typeof(defaultObject)!="object"||Array.isArray(defaultObject)){
        return {};
    }
   

    for(let key in defaultObject){
        if(testObject[key]==undefined){
            testObject[key]=defaultObject[key];
        }
    }
    return testObject;
    

}

module.exports = defaults;