function invert(testObject){
    if(testObject== null || testObject == undefined){
        return {};
    }
    if(typeof(testObject)!="object"){
        return {};
    }

    let invertObject ={}
    for(let key in testObject){
        newKey=testObject[key];
        invertObject[newKey]=key;
    }

    return invertObject;
}

module.exports=invert;