
function valuesOfObject(inputObject){
    let arrayOfValues=[];
    if(inputObject== null || inputObject == undefined){
        return keysAsArray;
    }
    if(typeof(inputObject)!="object"){
        return keysAsArray;
    }
    for(let key in inputObject){
        arrayOfValues.push(inputObject[key]);
    }
    return arrayOfValues;
}

module.exports = valuesOfObject;