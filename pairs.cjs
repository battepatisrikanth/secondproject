function pairs(testObject){
    if(testObject== null || testObject == undefined || typeof(testObject)!="object"){
        return [];
    }
    
    let arrayPair =[];
    let keyAsString='';
    for(let key in testObject){
        keyAsString = key;
        arrayPair.push([keyAsString,testObject[key]]);
    }
    return arrayPair;

}

module.exports=pairs;