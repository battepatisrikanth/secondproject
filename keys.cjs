
function keys(testObject){
    let keyAsString='';
    let keysArray=[];
    if(testObject== null || testObject == undefined){
        return keysArray;
    }
    if(typeof(testObject)!="object"){
        return keysArray;
    }
    for(let key in testObject){
        keyAsString=key;
        keysArray.push(keyAsString);
    }
    return keysArray;
}

module.exports=keys;